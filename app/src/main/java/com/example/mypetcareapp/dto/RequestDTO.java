package com.example.mypetcareapp.dto;

public class RequestDTO {
    private String idRequest;
    private String idUserRequest;
    private String idUserReceiver;
    private String status;

    public RequestDTO(String idUserRequest, String idUserReceiver) {
        this.idRequest = "";
        this.idUserRequest = idUserRequest;
        this.idUserReceiver = idUserReceiver;
        this.status = "Sin gestionar";
    }

    public String getIdRequest() {
        return idRequest;
    }

    public void setIdRequest(String idRequest) {
        this.idRequest = idRequest;
    }

    public String getIdUserRequest() {
        return idUserRequest;
    }

    public void setIdUserRequest(String idUserRequest) {
        this.idUserRequest = idUserRequest;
    }

    public String getIdUserReceiver() {
        return idUserReceiver;
    }

    public void setIdUserReceiver(String idUserReceiver) {
        this.idUserReceiver = idUserReceiver;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
