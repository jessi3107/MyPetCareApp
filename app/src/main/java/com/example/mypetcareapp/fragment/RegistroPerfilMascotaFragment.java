package com.example.mypetcareapp.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.example.mypetcareapp.R;
import com.example.mypetcareapp.RegistroDatosMascotaActivity;
import com.example.mypetcareapp.RegistroPerfilMascotaActivity;
import com.example.mypetcareapp.dto.DataBasicPetDTO;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class RegistroPerfilMascotaFragment extends Fragment implements View.OnClickListener {

    private Spinner spinnerTypeMore;
    private EditText txtNameMore;
    private Spinner spinnerGenderMore;
    private EditText txtRaceMore;
    private EditText txtHeightMore;
    private EditText txtWeightMore;
    private Spinner spinnerRegisterDataMore;
    private Button btnRegisterMore;
    private String idUser;
    private boolean isPetDataRegister;

    private View root;
    DatabaseReference mRootReference;
    private Context context;

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        if(getArguments() != null){
            idUser = getArguments().getString("id");
        }

        root = inflater.inflate(R.layout.activity_registro_perfil_mascota, container, false);

        context = root.getContext();

        // Conexión Firebase
        mRootReference = FirebaseDatabase.getInstance().getReference();

        spinnerTypeMore = (Spinner) root.findViewById(R.id.spinnerTypePet);
        txtNameMore = (EditText) root.findViewById(R.id.txtNamePet);
        spinnerGenderMore = (Spinner) root.findViewById(R.id.spinnerGenderPet);
        txtRaceMore = (EditText) root.findViewById(R.id.txtRacePet);
        txtHeightMore = (EditText) root.findViewById(R.id.txtHeightPet);
        txtWeightMore = (EditText) root.findViewById(R.id.txtWeightPet);
        spinnerRegisterDataMore = (Spinner) root.findViewById(R.id.spinnerRegisterPetData);

        ArrayAdapter adp_typePet = new ArrayAdapter(root.getContext(), android.R.layout.simple_spinner_dropdown_item, getTypePet());
        ArrayAdapter adp_genderPet = new ArrayAdapter(root.getContext(), android.R.layout.simple_spinner_dropdown_item, getGenderPet());
        ArrayAdapter adp_registerDataPet = new ArrayAdapter(root.getContext(), android.R.layout.simple_spinner_dropdown_item, getRegisterDataPet());

        spinnerTypeMore.setAdapter(adp_typePet);
        spinnerGenderMore.setAdapter(adp_genderPet);
        spinnerRegisterDataMore.setAdapter(adp_registerDataPet);

        // Logica si se selecciona alguna opción
        logicTypePet();
        logicGenderPet();
        logicRegisterDataPet();

        btnRegisterMore = (Button) root.findViewById(R.id.btnRegister);
        btnRegisterMore.setOnClickListener(this);

        return root;
    }

    @Override
    public void onClick(View view) {
        //Invocamos al método:
        registerPet();
    }

    //********************* Inicio de la Logica requerida para esta Actividad ****************************//

    public void nextActivityPetDataRegister(String idPet){
        Intent intent = new Intent(context.getApplicationContext(), RegistroDatosMascotaActivity.class);
        intent.putExtra("idUser", idUser);
        intent.putExtra("idPet", idPet);
        context.startActivity(intent);
    }

    private void registerPet(){

        final DataBasicPetDTO dataBasicPetDTO;
        try{
            dataBasicPetDTO = validateRegister();
        }catch (Exception exc){
            Toast.makeText(root.getContext(), exc.getMessage(),Toast.LENGTH_LONG).show();
            return;
        }

        generateId(dataBasicPetDTO);
    }

    private final DataBasicPetDTO validateRegister() throws Exception {

        final DataBasicPetDTO dataBasicPetDTO = new DataBasicPetDTO();

        String typeMore = spinnerTypeMore.getSelectedItem().toString().trim();
        String nameMore = txtNameMore.getText().toString().trim();
        String genderMore = spinnerGenderMore.getSelectedItem().toString().trim();
        String raceMore = txtRaceMore.getText().toString().trim();
        String heightMore = txtHeightMore.getText().toString().trim();
        String weightMore = txtWeightMore.getText().toString().trim();
        String registerDataMore = spinnerRegisterDataMore.getSelectedItem().toString().trim();

        if(typeMore.equals("Seleccione tipo de mascota") || TextUtils.isEmpty(typeMore)){
            throw new Exception("Se debe ingresar el tipo de mascota");
        }

        if(TextUtils.isEmpty(nameMore)){
            throw new Exception("Se debe ingresar el nombre de la mascota");
        }

        if(genderMore.equals("Seleccione genero de mascota") || TextUtils.isEmpty(genderMore)){
            throw new Exception("Se debe ingresar el genero de la mascota");
        }

        if(TextUtils.isEmpty(raceMore)){
            throw new Exception("Se debe ingresar la raza de la mascota");
        }

        if(TextUtils.isEmpty(heightMore)){
            throw new Exception("Se debe ingresar la altura de la mascota");
        }

        if(TextUtils.isEmpty(weightMore)){
            throw new Exception("Se debe ingresar el peso de la mascota");
        }

        if(registerDataMore.equals("Desea registrar los datos de su mascota") || TextUtils.isEmpty(registerDataMore)){
            throw new Exception("Debe indicar si desea registrar los datos mascota");
        }

        List<String> idUsers = new ArrayList<>();
        idUsers.add(idUser);

        dataBasicPetDTO.setIdUser(idUsers);
        dataBasicPetDTO.setName(nameMore);
        dataBasicPetDTO.setType(typeMore);
        dataBasicPetDTO.setGender(genderMore);
        dataBasicPetDTO.setRace(raceMore);
        dataBasicPetDTO.setHeight(heightMore);
        dataBasicPetDTO.setWeight(weightMore);
        isPetDataRegister = registerDataMore.equals("Si") ? true : false;

        return dataBasicPetDTO;
    }

    private ArrayList<String> getTypePet(){

        ArrayList<String> typePet = new ArrayList<>();

        typePet.add("Seleccione tipo de mascota");
        typePet.add("Perro");
        typePet.add("Gato");
        typePet.add("Caballo");
        typePet.add("Hamster");
        typePet.add("Ave");
        typePet.add("Pez");

        return typePet;
    }

    private ArrayList<String> getGenderPet(){

        ArrayList<String> genderPet = new ArrayList<>();

        genderPet.add("Seleccione genero de mascota");
        genderPet.add("Macho");
        genderPet.add("Hembra");

        return genderPet;
    }

    private ArrayList<String> getRegisterDataPet(){

        ArrayList<String> registerPet = new ArrayList<>();

        registerPet.add("Desea registrar los datos de su mascota");
        registerPet.add("Si");
        registerPet.add("No");

        return registerPet;
    }

    private void logicTypePet(){
        spinnerTypeMore.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String type = (String) spinnerTypeMore.getAdapter().getItem(position);
                if(!type.equals("Seleccione tipo de mascota")){
                    Toast.makeText(root.getContext(), "Seleccionaste: " + type, Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
    }

    private void logicGenderPet(){
        spinnerGenderMore.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String type = (String) spinnerGenderMore.getAdapter().getItem(position);
                if(!type.equals("Seleccione genero de mascota")){
                    Toast.makeText(root.getContext(), "Seleccionaste: " + type, Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
    }

    private void logicRegisterDataPet(){
        spinnerRegisterDataMore.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String type = (String) spinnerRegisterDataMore.getAdapter().getItem(position);
                if(!type.equals("Desea registrar los datos de su mascota")){
                    Toast.makeText(root.getContext(), "Seleccionaste: " + type, Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
    }

    //********************* Inicio solicitudes a Firebase requeridas para esta Actividad ****************************//

    private void generateId(final DataBasicPetDTO dataBasicPetDTO){
        //Instancia a la base de datos
        FirebaseDatabase fdb = FirebaseDatabase.getInstance();
        //apuntamos al nodo que queremos leer
        DatabaseReference myRef = fdb.getReference("mascota");

        myRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if(snapshot.exists()){
                    String id = String.valueOf(snapshot.getChildrenCount()+1);
                    String idUser = "";
                    for(long count = id.length(); count < 7; count++){
                        idUser = idUser+"0";
                    }
                    idUser = idUser+id;
                    dataBasicPetDTO.setId(idUser);
                }
                registerNow(dataBasicPetDTO);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                System.out.println("Fallo la lectura: " + error.getCode());
            }
        });
    }

    public void registerNow(final DataBasicPetDTO dataBasicPetDTO){

        try{
            mRootReference.child("mascota").push().setValue(dataBasicPetDTO);
            Toast toast = Toast.makeText(root.getContext(), "Se realizo el registro de la mascota: "+dataBasicPetDTO.getName(), Toast.LENGTH_SHORT);
            toast.show();
            if(isPetDataRegister){
                nextActivityPetDataRegister(dataBasicPetDTO.getId());
            }
        }catch (Exception ex){
            Toast toast = Toast.makeText(root.getContext(), "Error del Servidor - no se ha podido realizar el registro de la mascota", Toast.LENGTH_SHORT);
            toast.show();
        }

    }

}
