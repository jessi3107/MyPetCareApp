package com.example.mypetcareapp.dto;

import android.widget.EditText;

import java.util.List;

public class DataBasicPetDTO {
    private String id;
    private List<String> idUsers;
    private String type;
    private String name;
    private String gender;
    private String race;
    private String height;
    private String weight;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public List<String> getIdUsers() {
        return idUsers;
    }

    public void setIdUser(List<String> idUsers) {
        this.idUsers = idUsers;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getRace() {
        return race;
    }

    public void setRace(String race) {
        this.race = race;
    }

    public String getHeight() {
        return height;
    }

    public void setHeight(String height) {
        this.height = height;
    }

    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }
}
