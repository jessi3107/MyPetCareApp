package com.example.mypetcareapp;

import androidx.appcompat.app.AppCompatActivity;

import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.os.Bundle;

import java.util.ArrayList;
import java.util.List;

public class RegistroDatosMascotaActivity extends AppCompatActivity {

    public Spinner spinnerCirugia;
    public Spinner spinnerTipoCirugia;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro_datos_mascota);

        spinnerCirugia = (Spinner) findViewById(R.id.spnCirugia);
        spinnerTipoCirugia = (Spinner) findViewById(R.id.spnTipoCirugia);
        spinnerCirugia = (Spinner) findViewById(R.id.spnAlergia);

        ArrayList<String> cirugia = new ArrayList<>();
        ArrayList<String> tipoCirugia = new ArrayList<>();

        cirugia.add("Si");
        cirugia.add("No");

        tipoCirugia.add("Seleccione tipo de cirugia");
        tipoCirugia.add("Mayor");
        tipoCirugia.add("Menor");
        tipoCirugia.add("Emergencia");

        ArrayAdapter adp = new ArrayAdapter(RegistroDatosMascotaActivity.this, android.R.layout.simple_spinner_dropdown_item, cirugia);
        ArrayAdapter adp2 = new ArrayAdapter(RegistroDatosMascotaActivity.this, android.R.layout.simple_spinner_dropdown_item, tipoCirugia);

        spinnerCirugia.setAdapter(adp);
        spinnerTipoCirugia.setAdapter(adp2);


        spinnerCirugia.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String tipo = (String) spinnerCirugia.getAdapter().getItem(position);

                //Toast.makeText(RegistroPerfilMascotaActivity.this, "Seleccionaste: " + tipo, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spinnerTipoCirugia.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String tipo = (String) spinnerTipoCirugia.getAdapter().getItem(position);

                //Toast.makeText(RegistroPerfilMascotaActivity.this, "Seleccionaste: " + tipo, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }

}