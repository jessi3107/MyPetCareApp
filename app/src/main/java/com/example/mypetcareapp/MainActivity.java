package com.example.mypetcareapp;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Application;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;

import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.mypetcareapp.dto.CredentialDTO;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthUserCollisionException;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private EditText textEmail;
    private EditText textPassword;
    private Button btnLogin;

    private FirebaseAuth firebaseAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        firebaseAuth = FirebaseAuth.getInstance();

        textEmail = (EditText) findViewById(R.id.txtEmail);
        textPassword = (EditText) findViewById(R.id.txtPassword);

        btnLogin = (Button) findViewById(R.id.buttonSignIn);
        btnLogin.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        this.userLogin();
    }

    /** Called when the user taps the Send button */
    public void registerNow(View view) {
        // Do something in response to button
        Intent intent = new Intent(this, RegistroActivity.class);
        startActivity(intent);
    }

    //********************* Inicio de la Logica requerida para esta Actividad ****************************//

    private void userLogin() {

        CredentialDTO credentialDTO;
        try{
            credentialDTO = validateLogger();
        }catch (Exception exc){
            Toast.makeText(this, exc.getMessage(),Toast.LENGTH_LONG).show();
            return;
        }

        logger(credentialDTO);
    }

    public void nextActivity(String id){
        Intent intent = new Intent(getApplication(), MainHomeActivity.class);
        intent.putExtra("id", id);
        startActivity(intent);
    }

    public CredentialDTO validateLogger() throws Exception{
        CredentialDTO credentialDTO = new CredentialDTO();

        //Obtenemos el email y la contraseña desde las cajas de texto
        final String email = textEmail.getText().toString().trim();
        String password = textPassword.getText().toString().trim();

        //Verificamos que las cajas de texto no esten vacías
        if (TextUtils.isEmpty(email)) {//(precio.equals(""))
            throw new Exception("Se debe ingresar un email");
        }

        if (TextUtils.isEmpty(password)) {
            throw new Exception("Falta ingresar la contraseña");
        }

        credentialDTO.setEmail(email);
        credentialDTO.setPassword(password);

        return credentialDTO;
    }

    //********************* Inicio solicitudes a Firebase requeridas para esta Actividad ****************************//

    public void logger(final CredentialDTO credentialDTO){
        //loguear usuario
        firebaseAuth.signInWithEmailAndPassword(credentialDTO.getEmail(), credentialDTO.getPassword()).addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                //checking if success
                if (task.isSuccessful()) {
                    requestId(credentialDTO.getEmail());
                } else {
                    if (task.getException() instanceof FirebaseAuthUserCollisionException) {//si se presenta una colisión
                        Toast.makeText(MainActivity.this, "Contraseña o Correo Incorrectos", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(MainActivity.this, "Error del Servidor - no se pudo iniciar sesión", Toast.LENGTH_LONG).show();
                    }
                }
            }
        });
    }

    public void requestId(final String email){
        final String[] response = {""};
        //Instancia a la base de datos
        FirebaseDatabase fdb = FirebaseDatabase.getInstance();
        //apuntamos al nodo que queremos leer
        DatabaseReference myRef = fdb.getReference("usuario");

        myRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if(snapshot.exists()){
                    for(DataSnapshot data: snapshot.getChildren()){
                        if(data.child("email").getValue().equals(email)){
                            response[0] = (String) data.child("id").getValue();
                            break;
                        }
                    }
                    Toast.makeText(MainActivity.this, "Bienvenido: " + email.split("@")[0], Toast.LENGTH_LONG).show();
                    nextActivity(response[0]);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                System.out.println("Fallo la lectura: " + error.getCode());
            }
        });
    }
}