package com.example.mypetcareapp;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.mypetcareapp.dto.DataBasicDTO;
import com.example.mypetcareapp.fragment.AssociatePetProfileFragment;
import com.example.mypetcareapp.fragment.HomeFragment;
import com.example.mypetcareapp.fragment.ManageRequestFragment;
import com.example.mypetcareapp.fragment.PetProfileFragment;
import com.example.mypetcareapp.fragment.RegistroPerfilMascotaFragment;
import com.google.android.material.navigation.NavigationView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.core.view.GravityCompat;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

public class MainHomeActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    private AppBarConfiguration mAppBarConfiguration;
    private String idUser;
    
    private ActionBarDrawerToggle actionBarDrawerToggle;
    private FragmentManager fragmentManager;
    private FragmentTransaction fragmentTransaction;
    private DrawerLayout drawer;
    private Bundle bundle;
    private NavigationView navigationView;
    private Toolbar toolbar;

    private TextView txtUser;
    private TextView txtEmail;

    private FirebaseAuth firebaseAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_home);
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // Recuperamos Datos de la Actividad Anterior
        if(this.getIntent().getExtras() != null){
            Bundle data = this.getIntent().getExtras();
            idUser = data.getString("id");
        }else{
            idUser = "000002";
        }

        // Connection Firebase
        firebaseAuth = FirebaseAuth.getInstance();

        drawer = findViewById(R.id.drawer_layout);
        navigationView = findViewById(R.id.nav_view);

        navigationView.setNavigationItemSelectedListener(this);
        requestUser();
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        drawer.closeDrawer(GravityCompat.START);
        if(item.getItemId() == R.id.nav_home){
            HomeFragment homeFragment = new HomeFragment();
            homeFragment.setArguments(bundle);

            fragmentManager = getSupportFragmentManager();
            fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.content_main, homeFragment);
            fragmentTransaction.commit();
        }

        if(item.getItemId() == R.id.nav_pet_profile){
            PetProfileFragment petProfileFragment = new PetProfileFragment();
            petProfileFragment.setArguments(bundle);

            fragmentManager = getSupportFragmentManager();
            fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.content_main, petProfileFragment);
            fragmentTransaction.commit();
        }

        if(item.getItemId() == R.id.nav_manage_request){
            ManageRequestFragment manageRequestFragment = new ManageRequestFragment();
            manageRequestFragment.setArguments(bundle);

            fragmentManager = getSupportFragmentManager();
            fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.content_main, manageRequestFragment);
            fragmentTransaction.commit();
        }

        if(item.getItemId() == R.id.nav_pet_profile_associate){
            AssociatePetProfileFragment associatePetProfileFragment = new AssociatePetProfileFragment();
            associatePetProfileFragment.setArguments(bundle);

            fragmentManager = getSupportFragmentManager();
            fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.content_main, associatePetProfileFragment);
            fragmentTransaction.commit();
        }

        if(item.getItemId() == R.id.nav_registrar_perfil_mascota){
            RegistroPerfilMascotaFragment registroPerfilMascotaFragment = new RegistroPerfilMascotaFragment();
            registroPerfilMascotaFragment.setArguments(bundle);

            fragmentManager = getSupportFragmentManager();
            fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.content_main, registroPerfilMascotaFragment);
            fragmentTransaction.commit();
        }

        return false;
    }

    /** Called when the user taps the Send button */
    public void viewProfile(View view) {
        // Do something in response to button
        Intent intent = new Intent(this, PerfilUsuarioActivity.class);
        intent.putExtra("id", idUser);
        startActivity(intent);
    }

    //********************* Inicio de la Logica requerida para esta Actividad ****************************//

    public void changeNavHeaderData(String[] response){
        View header = navigationView.getHeaderView(0);
        txtUser = (TextView) header.findViewById(R.id.txtUser);
        txtEmail = (TextView) header.findViewById(R.id.textView);
        txtUser.setText(response[0]);
        txtEmail.setText(response[1]);
        startFragment();
    }

    private void startFragment(){
        actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.open, R.string.close);
        drawer.addDrawerListener(actionBarDrawerToggle);
        actionBarDrawerToggle.setDrawerIndicatorEnabled(true);
        actionBarDrawerToggle.syncState();

        bundle = new Bundle();
        bundle.putString("id", idUser);

        HomeFragment homeFragment = new HomeFragment();
        homeFragment.setArguments(bundle);

        fragmentManager = getSupportFragmentManager();
        fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.add(R.id.content_main, homeFragment);
        fragmentTransaction.commitAllowingStateLoss();
    }

    //********************* Inicio solicitudes a Firebase requeridas para esta Actividad ****************************//

    private void requestUser(){
        final String[] response = {"",""};
        //Instancia a la base de datos
        FirebaseDatabase fdb = FirebaseDatabase.getInstance();
        //apuntamos al nodo que queremos leer
        DatabaseReference myRef = fdb.getReference("usuario");

        myRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if(snapshot.exists()){
                    for(DataSnapshot data: snapshot.getChildren()){
                        if(data.child("id").getValue().equals(idUser)){
                            response[0] = (String) data.child("name").getValue();
                            response[1] = (String) data.child("email").getValue();
                            break;
                        }
                    }
                    changeNavHeaderData(response);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                System.out.println("Fallo la lectura: " + error.getCode());
            }
        });
    }
}