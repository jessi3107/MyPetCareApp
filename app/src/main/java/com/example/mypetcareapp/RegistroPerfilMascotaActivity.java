package com.example.mypetcareapp;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.mypetcareapp.dto.DataBasicDTO;
import com.example.mypetcareapp.dto.DataBasicPetDTO;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuthUserCollisionException;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class RegistroPerfilMascotaActivity extends AppCompatActivity implements View.OnClickListener {

    private Spinner spinnerTypeMore;
    private EditText txtNameMore;
    private Spinner spinnerGenderMore;
    private EditText txtRaceMore;
    private EditText txtHeightMore;
    private EditText txtWeightMore;
    private Spinner spinnerRegisterDataMore;
    private Button btnRegisterMore;
    private String idUser;
    private boolean isPetDataRegister;

    private Toolbar toolbar;
    DatabaseReference mRootReference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro_perfil_mascota);

        // Recuperamos Datos de la Actividad Anterior
        Bundle data = this.getIntent().getExtras();
        idUser = data.getString("id");

        // Creación de Toolbar
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // Conexión Firebase
        mRootReference = FirebaseDatabase.getInstance().getReference();

        spinnerTypeMore = (Spinner) findViewById(R.id.spinnerTypePet);
        txtNameMore = (EditText) findViewById(R.id.txtNamePet);
        spinnerGenderMore = (Spinner) findViewById(R.id.spinnerGenderPet);
        txtRaceMore = (EditText) findViewById(R.id.txtRacePet);
        txtHeightMore = (EditText) findViewById(R.id.txtHeightPet);
        txtWeightMore = (EditText) findViewById(R.id.txtWeightPet);
        spinnerRegisterDataMore = (Spinner) findViewById(R.id.spinnerRegisterPetData);

        ArrayAdapter adp_typePet = new ArrayAdapter(RegistroPerfilMascotaActivity.this, android.R.layout.simple_spinner_dropdown_item, getTypePet());
        ArrayAdapter adp_genderPet = new ArrayAdapter(RegistroPerfilMascotaActivity.this, android.R.layout.simple_spinner_dropdown_item, getGenderPet());
        ArrayAdapter adp_registerDataPet = new ArrayAdapter(RegistroPerfilMascotaActivity.this, android.R.layout.simple_spinner_dropdown_item, getRegisterDataPet());

        spinnerTypeMore.setAdapter(adp_typePet);
        spinnerGenderMore.setAdapter(adp_genderPet);
        spinnerRegisterDataMore.setAdapter(adp_registerDataPet);

        // Logica si se selecciona alguna opción
        logicTypePet();
        logicGenderPet();
        logicRegisterDataPet();

        btnRegisterMore = (Button) findViewById(R.id.btnRegister);
        btnRegisterMore.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        //Invocamos al método:
        registerPet();
    }

    //********************* Inicio de la Logica requerida para esta Actividad ****************************//

    private void registerPet(){

        final DataBasicPetDTO dataBasicPetDTO;
        try{
            dataBasicPetDTO = validateRegister();
        }catch (Exception exc){
            Toast.makeText(this, exc.getMessage(),Toast.LENGTH_LONG).show();
            return;
        }

        generateId(dataBasicPetDTO);
    }

    public void nextActivityMain(){
        Intent intent = new Intent(getApplication(), MainActivity.class);
        startActivity(intent);
    }

    public void nextActivityPetDataRegister(String idPet){
        Intent intent = new Intent(getApplication(), RegistroDatosMascotaActivity.class);
        intent.putExtra("idUser", idUser);
        intent.putExtra("idPet", idPet);
        startActivity(intent);
    }

    private final DataBasicPetDTO validateRegister() throws Exception {

        final DataBasicPetDTO dataBasicPetDTO = new DataBasicPetDTO();

        String typeMore = spinnerTypeMore.getSelectedItem().toString().trim();
        String nameMore = txtNameMore.getText().toString().trim();
        String genderMore = spinnerGenderMore.getSelectedItem().toString().trim();
        String raceMore = txtRaceMore.getText().toString().trim();
        String heightMore = txtHeightMore.getText().toString().trim();
        String weightMore = txtWeightMore.getText().toString().trim();
        String registerDataMore = spinnerRegisterDataMore.getSelectedItem().toString().trim();

        if(typeMore.equals("Seleccione tipo de mascota") || TextUtils.isEmpty(typeMore)){
            throw new Exception("Se debe ingresar el tipo de mascota");
        }

        if(TextUtils.isEmpty(nameMore)){
            throw new Exception("Se debe ingresar el nombre de la mascota");
        }

        if(genderMore.equals("Seleccione genero de mascota") || TextUtils.isEmpty(genderMore)){
            throw new Exception("Se debe ingresar el genero de la mascota");
        }

        if(TextUtils.isEmpty(raceMore)){
            throw new Exception("Se debe ingresar la raza de la mascota");
        }

        if(TextUtils.isEmpty(heightMore)){
            throw new Exception("Se debe ingresar la altura de la mascota");
        }

        if(TextUtils.isEmpty(weightMore)){
            throw new Exception("Se debe ingresar el peso de la mascota");
        }

        if(registerDataMore.equals("Desea registrar los datos de su mascota") || TextUtils.isEmpty(registerDataMore)){
            throw new Exception("Debe indicar si desea registrar los datos mascota");
        }

        List<String> idUsers = new ArrayList<>();
        idUsers.add(idUser);

        dataBasicPetDTO.setIdUser(idUsers);
        dataBasicPetDTO.setName(nameMore);
        dataBasicPetDTO.setType(typeMore);
        dataBasicPetDTO.setGender(genderMore);
        dataBasicPetDTO.setRace(raceMore);
        dataBasicPetDTO.setHeight(heightMore);
        dataBasicPetDTO.setWeight(weightMore);
        isPetDataRegister = registerDataMore.equals("Si") ? true : false;

        return dataBasicPetDTO;
    }

    private ArrayList<String> getTypePet(){

        ArrayList<String> typePet = new ArrayList<>();

        typePet.add("Seleccione tipo de mascota");
        typePet.add("Perro");
        typePet.add("Gato");
        typePet.add("Caballo");
        typePet.add("Hamster");
        typePet.add("Ave");
        typePet.add("Pez");

        return typePet;
    }

    private ArrayList<String> getGenderPet(){

        ArrayList<String> genderPet = new ArrayList<>();

        genderPet.add("Seleccione genero de mascota");
        genderPet.add("Macho");
        genderPet.add("Hembra");

        return genderPet;
    }

    private ArrayList<String> getRegisterDataPet(){

        ArrayList<String> registerPet = new ArrayList<>();

        registerPet.add("Desea registrar los datos de su mascota");
        registerPet.add("Si");
        registerPet.add("No");

        return registerPet;
    }

    private void logicTypePet(){
        spinnerTypeMore.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String type = (String) spinnerTypeMore.getAdapter().getItem(position);
                if(!type.equals("Seleccione tipo de mascota")){
                    Toast.makeText(RegistroPerfilMascotaActivity.this, "Seleccionaste: " + type, Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
    }

    private void logicGenderPet(){
        spinnerGenderMore.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String type = (String) spinnerGenderMore.getAdapter().getItem(position);
                if(!type.equals("Seleccione genero de mascota")){
                    Toast.makeText(RegistroPerfilMascotaActivity.this, "Seleccionaste: " + type, Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
    }

    private void logicRegisterDataPet(){
        spinnerRegisterDataMore.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String type = (String) spinnerRegisterDataMore.getAdapter().getItem(position);
                if(!type.equals("Desea registrar los datos de su mascota")){
                    Toast.makeText(RegistroPerfilMascotaActivity.this, "Seleccionaste: " + type, Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
    }

    //********************* Inicio solicitudes a Firebase requeridas para esta Actividad ****************************//

    private void generateId(final DataBasicPetDTO dataBasicPetDTO){
        //Instancia a la base de datos
        FirebaseDatabase fdb = FirebaseDatabase.getInstance();
        //apuntamos al nodo que queremos leer
        DatabaseReference myRef = fdb.getReference("mascota");

        myRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if(snapshot.exists()){
                    String id = String.valueOf(snapshot.getChildrenCount()+1);
                    String idUser = "";
                    for(long count = id.length(); count < 7; count++){
                        idUser = idUser+"0";
                    }
                    idUser = idUser+id;
                    dataBasicPetDTO.setId(idUser);
                }
                registerNow(dataBasicPetDTO);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                System.out.println("Fallo la lectura: " + error.getCode());
            }
        });
    }

    public void registerNow(final DataBasicPetDTO dataBasicPetDTO){

        try{
            mRootReference.child("mascota").push().setValue(dataBasicPetDTO);
            Toast toast = Toast.makeText(this, "Se realizo el registro de la mascota: "+dataBasicPetDTO.getName(), Toast.LENGTH_SHORT);
            toast.show();
            if(isPetDataRegister){
                nextActivityPetDataRegister(dataBasicPetDTO.getId());
            }else{
                nextActivityMain();
            }
        }catch (Exception ex){
            Toast toast = Toast.makeText(this, "Error del Servidor - no se ha podido realizar el registro de la mascota", Toast.LENGTH_SHORT);
            toast.show();
        }

    }
}