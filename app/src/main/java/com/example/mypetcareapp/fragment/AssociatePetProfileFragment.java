package com.example.mypetcareapp.fragment;

import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import android.annotation.SuppressLint;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.mypetcareapp.R;
import com.example.mypetcareapp.RegistroActivity;
import com.example.mypetcareapp.dto.CredentialDTO;
import com.example.mypetcareapp.dto.DataBasicDTO;
import com.example.mypetcareapp.dto.DataBasicPetDTO;
import com.example.mypetcareapp.dto.RequestDTO;
import com.example.mypetcareapp.dto.UserDTO;
import com.example.mypetcareapp.logic.CustomAdapter;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuthUserCollisionException;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class AssociatePetProfileFragment extends Fragment implements View.OnClickListener {

    private ListView listViewUser;
    private List<UserDTO> userDTOList;
    private List<String> idUsers;
    private String idUser;
    private String idPet;
    private Button btnSearch;
    private Button btnSendRequest;
    private EditText txtIdPet;
    private View root;

    DatabaseReference mRootReference;

    @SuppressLint("WrongConstant")
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        if(getArguments() != null){
            idUser = getArguments().getString("id");
        }

        root = inflater.inflate(R.layout.fragment_associate_pet_profile, container, false);

        // Conexión Firebase
        mRootReference = FirebaseDatabase.getInstance().getReference();

        listViewUser = (ListView) root.findViewById(R.id.listViewUsers);
        txtIdPet = (EditText) root.findViewById(R.id.txtIdPet);

        btnSearch = (Button) root.findViewById(R.id.btnSearch);
        btnSearch.setOnClickListener(this);

        btnSendRequest = (Button) root.findViewById(R.id.btnSendRequest);
        btnSendRequest.setEnabled(false);
        btnSendRequest.setVisibility(View.GONE);
        btnSendRequest.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                List<RequestDTO> response = new ArrayList<>();
                for(String id: idUsers){
                    response.add(new RequestDTO(idUser, id));
                }
                generateId(response);
            }
        });

        return root;
    }

    @Override
    public void onClick(View view) {
        try {
            validateId();
        }catch (Exception exc){
            Toast.makeText(root.getContext(), exc.getMessage(), Toast.LENGTH_LONG).show();
            return;
        }

    }

    //********************* Inicio de la Logica requerida para esta Actividad ****************************//

    private void createAdapter(){
        CustomAdapter adapter = new CustomAdapter(root.getContext(), userDTOList);
        listViewUser.setAdapter(adapter);
    }

    private void validateId() throws Exception{

        final String idPet = txtIdPet.getText().toString().trim();

        if (TextUtils.isEmpty(idPet)) {
            throw new Exception("Se debe ingresar un ID de mascota para realizar la busqueda");
        }

        this.idPet = idPet;

        requestIdUsers();
    }

    //********************* Inicio solicitudes a Firebase requeridas para esta Actividad ****************************//

    private void getData(List<String> idUsers) {
        userDTOList = new ArrayList<>();
        this.idUsers = idUsers;
        final List<String> idUsersBD = new ArrayList<>(idUsers);

        FirebaseDatabase fdb = FirebaseDatabase.getInstance();
        //apuntamos al nodo que queremos leer
        DatabaseReference myRef = fdb.getReference("usuario");

        myRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if(snapshot.exists()){
                    int cont = 1;
                    for(DataSnapshot data: snapshot.getChildren()){
                        if(idUsersBD.contains(data.child("id").getValue())){
                            userDTOList.add(new UserDTO(cont, (String) data.child("name").getValue() ,R.mipmap.ic_launcher_round));
                            idUsersBD.remove(data.child("id").getValue());
                            if(idUsersBD.size() == 0){
                                break;
                            }
                            cont++;
                        }
                    }
                    createAdapter();
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                System.out.println("Fallo la lectura: " + error.getCode());
            }
        });
    }

    private void requestIdUsers(){
        final List<String>[] response = new List[]{new ArrayList<>()};
        //Instancia a la base de datos
        FirebaseDatabase fdb = FirebaseDatabase.getInstance();
        //apuntamos al nodo que queremos leer
        DatabaseReference myRef = fdb.getReference("mascota");

        myRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if(snapshot.exists()){
                    for(DataSnapshot data: snapshot.getChildren()){
                        if(data.child("id").getValue().equals(idPet)){
                            response[0] = (List<String>) data.child("idUsers").getValue();
                            break;
                        }
                    }
                    if(response[0].size() > 0){
                        getData(response[0]);
                        btnSendRequest.setEnabled(true);
                        btnSendRequest.setVisibility(View.VISIBLE);
                    }else{
                        List<UserDTO> response = new ArrayList<>();
                        listViewUser.setAdapter(new CustomAdapter(root.getContext(), response));
                        btnSendRequest.setEnabled(false);
                        btnSendRequest.setVisibility(View.GONE);
                        Toast.makeText(root.getContext(), "No se encontro ninguna mascota asociada", Toast.LENGTH_LONG).show();
                        return;
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                System.out.println("Fallo la lectura: " + error.getCode());
            }
        });
    }

    private void generateId(final List<RequestDTO> requestDTOList){
        //Instancia a la base de datos
        FirebaseDatabase fdb = FirebaseDatabase.getInstance();
        //apuntamos al nodo que queremos leer
        DatabaseReference myRef = fdb.getReference("solicitud");

        myRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                for(int i = 0; i < requestDTOList.size(); i++){
                    String idRequest = "";
                    String id = String.valueOf(i+1);
                    if(snapshot.exists()){
                        id = String.valueOf(snapshot.getChildrenCount()+i+1);
                    }
                    for(long count = id.length(); count < 6; count++){
                        idRequest = idRequest+"0";
                    }
                    idRequest = idRequest+id;
                    requestDTOList.get(i).setIdRequest(idRequest);
                }
                createRequest(requestDTOList);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                System.out.println("Fallo la lectura: " + error.getCode());
            }
        });
    }

    public void createRequest(final List<RequestDTO> requestDTOList){

        try{
            for(int i = 0; i < requestDTOList.size(); i++){
                mRootReference.child("solicitud").push().setValue(requestDTOList.get(i));
            }
            Toast toast = Toast.makeText(root.getContext(), "Se realizo el envio de la solicitud correctamente", Toast.LENGTH_SHORT);
            toast.show();
        }catch (Exception ex){
            Toast toast = Toast.makeText(root.getContext(), "Error del Servidor - no se ha podido realizar el envio de la solicitud", Toast.LENGTH_SHORT);
            toast.show();
        }

    }

}