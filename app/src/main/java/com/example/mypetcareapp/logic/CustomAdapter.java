package com.example.mypetcareapp.logic;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.mypetcareapp.R;
import com.example.mypetcareapp.dto.UserDTO;
import com.example.mypetcareapp.fragment.AssociatePetProfileFragment;

import java.util.List;

public class CustomAdapter extends BaseAdapter {

    Context context;
    List<UserDTO> userDTOS;

    public CustomAdapter(Context context, List<UserDTO> userDTOS) {
        this.context = context;
        this.userDTOS = userDTOS;
    }

    @Override
    public int getCount() {
        return userDTOS.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ImageView imageViewUser;
        TextView textViewUserName;

        UserDTO user = userDTOS.get(position);

        if(convertView == null){
            convertView = LayoutInflater.from(context).inflate(R.layout.list_view_usuarios, null);
        }

        imageViewUser = convertView.findViewById(R.id.imageViewUser);
        textViewUserName = convertView.findViewById(R.id.textViewNameUser);

        imageViewUser.setImageResource(user.getImage());
        textViewUserName.setText(user.getName());

        return convertView;
    }
}
