package com.example.mypetcareapp;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.mypetcareapp.dto.DataBasicDTO;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthUserCollisionException;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class RegistroActivity extends AppCompatActivity implements View.OnClickListener{

    private EditText txtName;
    private EditText txtTelephone;
    private EditText txtEmail;
    private EditText txtPassword;
    private EditText txtConfirmPassword;
    private Spinner spinnerPetRegister;
    private Button btnLogin;
    private boolean isPetRegister;

    private FirebaseAuth firebaseAuth;
    DatabaseReference mRootReference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // Get a support ActionBar corresponding to this toolbar
        ActionBar actionBar = getSupportActionBar();

        // Enable the Up button
        actionBar.setDisplayHomeAsUpEnabled(true);

        // Connection Firebase
        firebaseAuth = FirebaseAuth.getInstance();
        mRootReference = FirebaseDatabase.getInstance().getReference();

        txtName = (EditText) findViewById(R.id.txtName);
        txtEmail = (EditText) findViewById(R.id.txtEmail);
        txtTelephone = (EditText) findViewById(R.id.txtTelephone);
        txtPassword = (EditText) findViewById(R.id.txtPassword);
        txtConfirmPassword = (EditText) findViewById(R.id.txtConfirmPassword);
        spinnerPetRegister = (Spinner) findViewById(R.id.spinnerPetRegister);

        ArrayAdapter adp_registerPet = new ArrayAdapter(RegistroActivity.this, android.R.layout.simple_spinner_dropdown_item, getRegisterPet());

        spinnerPetRegister.setAdapter(adp_registerPet);

        logicRegisterPet();

        btnLogin = (Button) findViewById(R.id.btnLogin);
        btnLogin.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        //Invocamos al método:
        userRegister();
    }

    //********************* Inicio de la Logica requerida para esta Actividad ****************************//

    private void userRegister(){

        final DataBasicDTO dataBasicDTO;
        try{
            dataBasicDTO = validateRegister();
        }catch (Exception exc){
            Toast.makeText(this, exc.getMessage(),Toast.LENGTH_LONG).show();
            return;
        }

        generateId(dataBasicDTO);
    }

    public void nextActivityMain(){
        Intent intent = new Intent(getApplication(), MainActivity.class);
        startActivity(intent);
    }

    public void nextActivityPetRegister(String id){
        Intent intent = new Intent(getApplication(), RegistroPerfilMascotaActivity.class);
        intent.putExtra("id", id);
        startActivity(intent);
    }

    private final DataBasicDTO validateRegister() throws Exception {

        final DataBasicDTO dataBasicDTO = new DataBasicDTO();

        String name = txtName.getText().toString().trim();
        String telephone = txtTelephone.getText().toString().trim();
        String email = txtEmail.getText().toString().trim();
        String password = txtPassword.getText().toString().trim();
        String confirmPassword = txtConfirmPassword.getText().toString().trim();
        String registerPet = spinnerPetRegister.getSelectedItem().toString().trim();

        if(TextUtils.isEmpty(name)){
            throw new Exception("Se debe ingresar el nombre");
        }

        if(TextUtils.isEmpty(telephone)){
            throw new Exception("Se debe ingresar el telefono");
        }

        if(TextUtils.isEmpty(email)){
            throw new Exception("Se debe ingresar un correo electronico");
        }

        if(TextUtils.isEmpty(password)){
            throw new Exception("Se debe ingresar la contraseña");
        }


        if(TextUtils.isEmpty(confirmPassword)){
            throw new Exception("Se debe confirmar la contraseña");
        }

        if(TextUtils.equals(password, confirmPassword)==false) {
            throw new Exception("Las contraseñas no coinciden");
        }

        if(registerPet.equals("Desea registrar su mascota") || TextUtils.isEmpty(registerPet)){
            throw new Exception("Debe indicar si desea registrar su mascota");
        }

        dataBasicDTO.setName(name);
        dataBasicDTO.setTelephone(telephone);
        dataBasicDTO.setEmail(email);
        dataBasicDTO.setPassword(password);
        dataBasicDTO.setUser(email.split("@")[0]);
        isPetRegister = registerPet.equals("Si") ? true : false;

        return dataBasicDTO;
    }

    private ArrayList<String> getRegisterPet(){

        ArrayList<String> registerPet = new ArrayList<>();

        registerPet.add("Desea registrar su mascota");
        registerPet.add("Si");
        registerPet.add("No");

        return registerPet;
    }

    private void logicRegisterPet(){
        spinnerPetRegister.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String type = (String) spinnerPetRegister.getAdapter().getItem(position);
                if(!type.equals("Desea registrar su mascota")){
                    Toast.makeText(RegistroActivity.this, "Seleccionaste: " + type, Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
    }


    //********************* Inicio solicitudes a Firebase requeridas para esta Actividad ****************************//

    private void generateId(final DataBasicDTO dataBasicDTO){
        //Instancia a la base de datos
        FirebaseDatabase fdb = FirebaseDatabase.getInstance();
        //apuntamos al nodo que queremos leer
        DatabaseReference myRef = fdb.getReference("usuario");

        myRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if(snapshot.exists()){
                    String id = String.valueOf(snapshot.getChildrenCount()+1);
                    String idUser = "";
                    for(long count = id.length(); count < 6; count++){
                        idUser = idUser+"0";
                    }
                    idUser = idUser+id;
                    dataBasicDTO.setId(idUser);
                }
                registerNow(dataBasicDTO);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                System.out.println("Fallo la lectura: " + error.getCode());
            }
        });
    }

    public void registerNow(final DataBasicDTO dataBasicDTO){
        firebaseAuth.createUserWithEmailAndPassword(dataBasicDTO.getEmail(), dataBasicDTO.getPassword()).addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {

                //checking if success
                if(task.isSuccessful()){
                    Toast.makeText(RegistroActivity.this,"Se ha registrado el usuario: " + dataBasicDTO.getUser(),Toast.LENGTH_LONG).show();
                }else{
                    if(task.getException() instanceof FirebaseAuthUserCollisionException) {//si se presenta una colisión
                        Toast.makeText(RegistroActivity.this, "El usuario \""+ dataBasicDTO.getUser() +"\" ya se encuentra registrado ", Toast.LENGTH_SHORT).show();
                        return;
                    } else {
                        Toast.makeText(RegistroActivity.this, "Error del Servidor - no se pudo registrar el usuario", Toast.LENGTH_LONG).show();
                        return;
                    }
                }
                mRootReference.child("usuario").push().setValue(dataBasicDTO);
                if(isPetRegister){
                    nextActivityPetRegister(dataBasicDTO.getId());
                }else{
                    nextActivityMain();
                }
            }
        });
    }

}