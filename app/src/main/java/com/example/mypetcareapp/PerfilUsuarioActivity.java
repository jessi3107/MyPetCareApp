package com.example.mypetcareapp;

import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;

import com.example.mypetcareapp.dto.DataBasicDTO;
import com.example.mypetcareapp.dto.DataBasicPetDTO;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.provider.ContactsContract;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.HashMap;
import java.util.Map;

public class PerfilUsuarioActivity extends AppCompatActivity implements View.OnClickListener {

    private String idUser;

    private EditText txtName;
    private EditText txtUser;
    private EditText txtTelephone;
    private EditText txtEmail;
    private EditText txtPassword;
    private EditText txtConfirmPassword;
    private CheckBox checkBox;
    private Button btnSave;
    private DataBasicDTO dataBasicDTO;

    DatabaseReference mRootReference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_perfil_usuario);

        // Recuperamos Datos de la Actividad Anterior
        Bundle data = this.getIntent().getExtras();
        idUser = data.getString("id");

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        mRootReference = FirebaseDatabase.getInstance().getReference();

        // Get a support ActionBar corresponding to this toolbar
        ActionBar actionBar = getSupportActionBar();

        // Enable the Up button
        actionBar.setDisplayHomeAsUpEnabled(true);

        txtName = (EditText) findViewById(R.id.txtName);
        txtUser = (EditText) findViewById(R.id.txtUser);
        txtEmail = (EditText) findViewById(R.id.txtEmail);
        txtTelephone = (EditText) findViewById(R.id.txtTelephone);
        txtPassword = (EditText) findViewById(R.id.txtPassword);
        txtConfirmPassword = (EditText) findViewById(R.id.txtConfirmPassword);
        checkBox = (CheckBox) findViewById(R.id.checkBox);

        requestUser();
        txtEmail.setEnabled(false);
        txtPassword.setEnabled(false);
        txtConfirmPassword.setEnabled(false);

        btnSave = (Button) findViewById(R.id.btnSave);
        btnSave.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        //Invocamos al método:
        userUpdate();
    }

    //********************* Inicio de la Logica requerida para esta Actividad ****************************//

    private void userUpdate(){

        final DataBasicDTO dataBasicDTO;
        try{
            dataBasicDTO = validateUpdate();
        }catch (Exception exc){
            Toast.makeText(this, exc.getMessage(),Toast.LENGTH_LONG).show();
            return;
        }

        updateDataUser(dataBasicDTO);
    }

    private final DataBasicDTO validateUpdate() throws Exception {

        final DataBasicDTO dataUpdate = new DataBasicDTO();

        String name = txtName.getText().toString().trim();
        String user = txtUser.getText().toString().trim();
        String telephone = txtTelephone.getText().toString().trim();
        String password = txtPassword.getText().toString().trim();
        String confirmPassword = txtConfirmPassword.getText().toString().trim();

        if(TextUtils.isEmpty(name)){
            throw new Exception("El campo \"Nombre\" no puede estar vaci");
        }

        if(TextUtils.isEmpty(user)){
            throw new Exception("El campo \"Usuario\" no puede estar vacio");
        }

        if(TextUtils.isEmpty(telephone)){
            throw new Exception("El campo \"Telefono\" no puede estar vacio");
        }

        if(TextUtils.isEmpty(password) && checkBox.isChecked()){
            throw new Exception("Se debe ingresar la contraseña");
        }


        if(TextUtils.isEmpty(confirmPassword) && checkBox.isChecked()){
            throw new Exception("Se debe confirmar la contraseña");
        }

        if(TextUtils.equals(password, confirmPassword)==false && checkBox.isChecked()) {
            throw new Exception("Las contraseñas no coinciden");
        }

        dataUpdate.setName(dataBasicDTO.getName().equals(name) ? null : name);
        dataUpdate.setUser(dataBasicDTO.getUser().equals(user) ? null : user);
        dataUpdate.setTelephone(dataBasicDTO.getTelephone().equals(telephone) ? null : telephone);
        dataUpdate.setPassword(dataBasicDTO.getPassword().equals(password) ? null : password);

        return dataUpdate;
    }

    public void changeProfileData(){
        txtName.setText(dataBasicDTO.getName());
        txtUser.setText(dataBasicDTO.getUser());
        txtEmail.setText(dataBasicDTO.getEmail());
        txtTelephone.setText(dataBasicDTO.getTelephone());
        txtPassword.setText(dataBasicDTO.getPassword());
        txtConfirmPassword.setText(dataBasicDTO.getPassword());
    }

    public void updatePassword(View view){
        if(checkBox.isChecked()){
            txtPassword.setEnabled(true);
            txtConfirmPassword.setEnabled(true);
        }else{
            txtPassword.setEnabled(false);
            txtConfirmPassword.setEnabled(false);
        }
    }

    //********************* Inicio solicitudes a Firebase requeridas para esta Actividad ****************************//

    private void requestUser(){
        final String[] response = {"",""};
        dataBasicDTO = new DataBasicDTO();
        //Instancia a la base de datos
        FirebaseDatabase fdb = FirebaseDatabase.getInstance();
        //apuntamos al nodo que queremos leer
        DatabaseReference myRef = fdb.getReference("usuario");

        myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if(snapshot.exists()){
                    for(DataSnapshot data: snapshot.getChildren()){
                        if(data.child("id").getValue().equals(idUser)){
                            dataBasicDTO.setId((String) data.getKey());
                            dataBasicDTO.setName((String) data.child("name").getValue());
                            dataBasicDTO.setUser((String) data.child("user").getValue());
                            dataBasicDTO.setEmail((String) data.child("email").getValue());
                            dataBasicDTO.setTelephone((String) data.child("telephone").getValue());
                            dataBasicDTO.setPassword((String) data.child("password").getValue());
                            break;
                        }
                    }
                    changeProfileData();
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                System.out.println("Fallo la lectura: " + error.getCode());
            }
        });
    }

    public void updateDataUser(DataBasicDTO updateData){

        try{
            Map<String, Object> updateDataUser = new HashMap<>();
            if(updateData.getName() != null){
                updateDataUser.put("name", updateData.getName());
            }

            if(updateData.getUser() != null){
                updateDataUser.put("user", updateData.getUser());
            }

            if(updateData.getTelephone() != null){
                updateDataUser.put("telephone", updateData.getTelephone());
            }

            if(updateData.getPassword() != null){
                updateDataUser.put("password", updateData.getPassword());
            }

            if(updateDataUser.size() > 0){

                mRootReference = FirebaseDatabase.getInstance().getReference("usuario");
                mRootReference.child(dataBasicDTO.getId()).updateChildren(updateDataUser);

                Toast toast = Toast.makeText(this, "Se realizo la actualización de sus Datos de forma correcta", Toast.LENGTH_SHORT);
                toast.show();
            }
        }catch (Exception ex){
            Toast toast = Toast.makeText(this, "Error del Servidor - no se ha podido realizar el registro de la mascota", Toast.LENGTH_SHORT);
            toast.show();
        }

    }
}